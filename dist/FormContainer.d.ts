import React from 'react';
declare const FormContainer: ({ onSuccess, defaultValues, ...rest }: React.PropsWithChildren<{
    onSuccess: any;
    defaultValues: {};
}>) => JSX.Element;
export default FormContainer;
